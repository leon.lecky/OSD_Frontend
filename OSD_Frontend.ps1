﻿#GatheringResources________________________________________________________________________________________________
    
    [reflection.assembly]::loadwithpartialname('System.Windows.Forms')
    [reflection.assembly]::loadwithpartialname('System.Drawing')
    #CurrentDirectory
    $PSScriptRoot = Split-Path -Parent -Path $MyInvocation.MyCommand.Definition
    #ImageBackground
    $imgbanner = [System.Drawing.Image]::Fromfile($PSScriptRoot+'\bin\logo.jpg')
    $Tick = [System.Drawing.Image]::Fromfile($PSScriptRoot+'\bin\3tick.jpg')
    $Cross = [System.Drawing.Image]::Fromfile($PSScriptRoot+'\bin\3cross.jpg')
    $blank = [System.Drawing.Image]::Fromfile($PSScriptRoot+'\bin\1blank.jpg')
#EndOfGatheringResources___________________________________________________________________________________________

#User Generated Script_____________________________________________________________________________________________

#Connect to Azure AD_______________________________________________________________________________________________

#$secureString = convertto-securestring "T3stF1st544" -asplaintext -force
#$azureAccountName ="349a9eac-2e02-463d-b217-a7dd84107b3f"
#$psCred = New-Object System.Management.Automation.PSCredential($azureAccountName, $secureString)
 
#Add-AzureRMAccount -Credential $psCred -TenantId f0d1c6fd-dff0-486a-8e91-cfefefc7d98d -ServicePrincipal
#Connect-AzureAD

#Connect to MDT DB____________________________________________________________________________________________________

#import-module -Name C:\test\PS_Modules\EF_MDTDB.psm1
#Connect-MDTDatabase -sqlServer USB-SSCMPSS01 -database MDT_Workspace_v2

<#Function Generated Script_____________________________________________________________________________________________

Function Get-RecursiveAzureAdGroupMemberUsers{
[cmdletbinding()]
param(
   [parameter(Mandatory=$True,ValueFromPipeline=$true)]
   $AzureGroup
)
    Begin{
        If(-not(Get-AzureADCurrentSessionInfo)){Connect-AzureAD}
    }
    Process {
        Write-Verbose -Message "Enumerating $($AzureGroup.DisplayName)"
        $Members = Get-AzureADGroupMember -ObjectId $AzureGroup.ObjectId -All $true
        
        $UserMembers = $Members | Where-Object{$_.ObjectType -eq 'user'}
        If($Members | Where-Object{$_.ObjectType -eq 'Group'}){
            $UserMembers += $Members | Where-Object{$_.ObjectType -eq 'Group'} | ForEach-Object{ Get-RecursiveAzureAdGroupMemberUsers -AzureGroup $_}
        }
    }
    end {
        Return $UserMembers
    }
}


#EndOfFunction Generated Script_____________________________________________________________________________________________#>

#User Generated Script_____________________________________________________________________________________________



#EndOfUserGeneratedScripts_________________________________________________________________________________________


#ElementsInWindows_________________________________________________________________________________________________

    $WelcomeWindow = New-Object -TypeName Windows.Forms.Form
    $WelcomeBanner = New-Object -TypeName Windows.Forms.PictureBox
    $cross1 = New-Object -TypeName Windows.Forms.PictureBox
    $cross2 = New-Object -TypeName Windows.Forms.PictureBox
    $cross3 = New-Object -TypeName Windows.Forms.PictureBox
    $cross4 = New-Object -TypeName Windows.Forms.PictureBox
    $tick1 = New-Object -TypeName Windows.Forms.PictureBox
    $tick2 = New-Object -TypeName Windows.Forms.PictureBox
    $tick3 = New-Object -TypeName Windows.Forms.PictureBox
    $tick4 = New-Object -TypeName Windows.Forms.PictureBox
    $buttonStart = New-Object -TypeName Windows.Forms.Button
    $buttonCancel = New-Object -TypeName Windows.Forms.Button
    #$buttonTraditional = New-Object -TypeName Windows.Forms.Button
    $MessageText1 = New-Object -TypeName Windows.Forms.Label
    $MessageText2 = New-Object -TypeName Windows.Forms.Label
    $MessageText3 = New-Object -TypeName Windows.Forms.Label
    $MessageTextbox1 = New-Object -TypeName Windows.Forms.TextBox
    $MessageTextbox2 = New-Object -TypeName Windows.Forms.TextBox
    $CheckBox =  New-Object System.Windows.Forms.Checkbox
    $OutputBox1 = New-Object System.Windows.Forms.label
    $OutputBox2 = New-Object System.Windows.Forms.label
    $OutputBox3 = New-Object System.Windows.Forms.label
    $OutputBox4 = New-Object System.Windows.Forms.label
    $Label1 = New-Object -TypeName Windows.Forms.Label
    $Label2 = New-Object -TypeName Windows.Forms.Label
    $Label3 = New-Object -TypeName Windows.Forms.Label
    $Label4 = New-Object -TypeName Windows.Forms.Label

#EndOfElementsInWindows____________________________________________________________________________________________



#MainWindow________________________________________________________________________________________________________
    
    $WelcomeWindow.WindowState = 'Normal'
    $WelcomeWindow.StartPosition = 'CenterScreen'
    $WelcomeWindow.TopMost = $True
    $WelcomeWindow.Name = 'Application Updates'
    $WelcomeWindow.Width = 600
    $WelcomeWindow.Height = 650
    $WelcomeWindow.BackColor = 'White'
    $WelcomeWindow.MinimizeBox = $False
    $WelcomeWindow.MaximizeBox = $False
    $WelcomeWindow.ControlBox = $False
    $WelcomeWindow.ShowInTaskbar = $False
    $WelcomeWindow.FormBorderStyle = [System.Windows.Forms.FormBorderStyle]::FixedSingle

#EndOfMainWindow___________________________________________________________________________________________________

#AddElementsInMainWindow___________________________________________________________________________________________
    
    $WelcomeWindow.Controls.Add($WelcomeBanner)
    $WelcomeWindow.Controls.Add($MessageText1)
    $WelcomeWindow.Controls.Add($MessageText2)
    $WelcomeWindow.Controls.Add($MessageText3)
    $WelcomeWindow.Controls.Add($buttonStart)
    $WelcomeWindow.Controls.Add($buttonCancel)
    #$WelcomeWindow.Controls.Add($buttonTraditional)
    $WelcomeWindow.Controls.Add($MessageTextbox1)
    $WelcomeWindow.Controls.Add($MessageTextbox2)
    $WelcomeWindow.Controls.Add($CheckBox)
    $WelcomeWindow.Controls.Add($OutputBox1)
    $WelcomeWindow.Controls.Add($OutputBox2)
    $WelcomeWindow.Controls.Add($OutputBox3)
    $WelcomeWindow.Controls.Add($OutputBox4)
    $WelcomeWindow.Controls.Add($Label1)
    $WelcomeWindow.Controls.Add($Label2)
    $WelcomeWindow.Controls.Add($Label3)
    $WelcomeWindow.Controls.Add($Label4)
    $WelcomeWindow.Controls.Add($cross1)
    $WelcomeWindow.Controls.Add($cross2)
    $WelcomeWindow.Controls.Add($cross3)
    $WelcomeWindow.Controls.Add($cross4)
    $WelcomeWindow.Controls.Add($tick1)
    $WelcomeWindow.Controls.Add($tick2)
    $WelcomeWindow.Controls.Add($tick3)
    $WelcomeWindow.Controls.Add($tick4)


#EndOfAddElementsInMainWindow______________________________________________________________________________________

#BackgroundImage___________________________________________________________________________________________________

    [System.Windows.Forms.Application]::EnableVisualStyles()
    $WelcomeBanner.DataBindings.DefaultDataSourceUpdateMode = 0
    $WelcomeBanner.Location =  New-Object System.Drawing.Size(90,0)
    $WelcomeBanner.Width = $imgbanner.Size.Width
    $WelcomeBanner.Height = 90
    $WelcomeBanner.Image = $imgbanner

#EndOfBackgroundImage______________________________________________________________________________________________



#MessageText1______________________________________________________________________________________________________

    $MessageText1.Font = New-Object System.Drawing.Font('Verdana',13,[System.Drawing.FontStyle]::Bold)
    $MessageText1.AutoSize = $True
    $MessageText1.Location = New-Object System.Drawing.Size(165,95)
    $MessageText1.ForeColor = "navy"
    $MessageText1.Text = "Cloud Workspace Install"

#EndOfMessageText1_________________________________________________________________________________________________



#MessageText2______________________________________________________________________________________________________
  
    $MessageText2.Font = New-Object System.Drawing.Font('Verdana',9,[System.Drawing.FontStyle]::Bold)
    $MessageText2.AutoSize = $True
    $MessageText2.Location = New-Object System.Drawing.Size(45,150)
    $MessageText2.ForeColor = "navy"
    $MessageText2.Text = "User Name: "

#EndOfMessageText2_________________________________________________________________________________________________

#MessageTextbox1___________________________________________________________________________________________________
  
    $MessageTextbox1.AutoSize = $True
    $MessageTextbox1.Location = New-Object System.Drawing.Size(175,150)
    $MessageTextbox1.size = New-Object System.Drawing.Size(300,400)
    $MessageTextbox1.ForeColor = "navy"
   

#EndOfMessageTextbox1_______________________________________________________________________________________________


#MessageText3______________________________________________________________________________________________________

    $MessageText3.Font = New-Object System.Drawing.Font('Verdana',9,[System.Drawing.FontStyle]::BOLD)
    $MessageText3.AutoSize = $True
    $MessageText3.Location = New-Object System.Drawing.Size(45,180)
    $MessageText3.ForeColor = "navy"
    $MessageText3.Text = "Computer Name:"

#EndOfMessageText3_________________________________________________________________________________________________

#MessageTextbox2___________________________________________________________________________________________________
  
    $MessageTextbox2.AutoSize = $True
    $MessageTextbox2.Location = New-Object System.Drawing.Size(175,180)
    $MessageTextbox2.size = New-Object System.Drawing.Size(300,400)
    $MessageTextbox2.ForeColor = "navy"
    $ComputerName = 'Default Name'
    $MessageTextbox2.Text = $ComputerName
    $MessageTextbox2.Enabled = $False

#EndOfMessageTextbox1_______________________________________________________________________________________________


#CheckBox__________________________________________________________________________________________________________
    
    $CheckBox.Location = New-Object System.Drawing.Size(45,540) 
    $CheckBox.Size = New-Object System.Drawing.Size(500,30)
    $CheckBox.Text = "I acknowledge that my computer's local hard drive will be wiped and I have backed up all my required data to my user drive."
    $CheckBox.TabIndex = 4
    $CheckBox_OnClick =
{ 
    #Start button will only be enabled if checkbox has been checked
    #$buttonStart.Enabled = $CheckBox.Checked

       
    if ($CheckBox.Checked -and $MessageTextbox1.Text.Length)
    {
    
    
    $body = @{

         username = $MessageTextbox1.Text;
         token = "6gDfq8tUqJnw&BY!";
          } | ConvertTo-Json

    $UserInfo = ConvertFrom-Json (Invoke-WebRequest -Uri https://4aubqt3iif.execute-api.eu-west-1.amazonaws.com/production/ef-identity-api-get-user-details -Body $body -Method Post -UseBasicParsing | Select-Object -Expand Content)
    write-host $MessageTextbox1.Text
    write-host $UserInfo.member

    $URI = "http://sswebsvc.ef.com/htaws_U12/htaws.asmx"
    $Web = New-WebServiceProxy -Uri $URI
    $MacAddress = Get-WmiObject win32_networkadapterconfiguration | select description, macaddress
    $MacAddress = Get-NetAdapter | select Name,MacAddress | where-Object {$_.name -eq "ethernet"}
    $MacAddress.MacAddress
    $UUID = Get-WmiObject -Class Win32_ComputerSystemProduct | Select-Object -Property UUID
    $UUID.UUID
    $sitecode = $userinfo.sitecode
    $GatherSerialKey = Get-WmiObject win32_bios | select SerialNumber
    $SerialKey = $GatherSerialKey.SerialNumber

    #To determine whether a machine is Laptop or not.
    Function Detect-Laptop
    {
    Param( [string]$computer = “localhost” )
    $isLaptop = $false
    #The chassis is the physical container that houses the components of a computer. Check if the machine’s chasis type is 9.Laptop 10.Notebook 14.Sub-Notebook
    if(Get-WmiObject -Class win32_systemenclosure -ComputerName $computer | Where-Object { $_.chassistypes -eq 9 -or $_.chassistypes -eq 10 -or $_.chassistypes -eq 14})
    { $isLaptop = $true }
    #Shows battery status , if true then the machine is a laptop.
    if(Get-WmiObject -Class win32_battery -ComputerName $computer)
    { $isLaptop = $true }
    $isLaptop
    }
    If(Detect-Laptop) { $Type = 'L'}
    else { $Type = 'W'}

    #Use Webservice to add machines to the collection
    $web.GenerateComputerName_v2($sitecode , $Type , $SerialKey , "DEFAULT")
    $Hostname = $web.GenerateComputerName_v2($sitecode , $Type , $SerialKey , "DEFAULT")
    $web.AddComputerToCollection($MacAddress.MacAddress , $UUID.UUID , "U1200296" , $Hostname)

    #When checkbox has been un-ticked after been ticked for the first time, it runs a removal of images forms. 
    #Therefore the images forms need to be added back when checkbox is checked again
    $WelcomeWindow.Controls.Add($cross1)
    $WelcomeWindow.Controls.Add($cross2)
    $WelcomeWindow.Controls.Add($cross3)
    $WelcomeWindow.Controls.Add($cross4)
    $WelcomeWindow.Controls.Add($tick1)
    $WelcomeWindow.Controls.Add($tick2)
    $WelcomeWindow.Controls.Add($tick3)
    $WelcomeWindow.Controls.Add($tick4)

    #Set Computer name
    
    

    <# #Variable to store what user types into textbox
      #$TextBoxInput = $MessageTextbox1.Text
        $totalusers = Get-AzureADGroup -SearchString "WorkstationIntuneTest" | Get-RecursiveAzureAdGroupMemberUsers | select objectid #"Global_AAD_Joined_Users"
        $UserAD_ObjectID = Get-AzureADUser -SearchString $TextBoxInput | select objectid
        $Global_AAD_Joined_Users_ObjectID =  "a15445b7-dae3-441b-98fc-d23f2a433ea9" #"9954dedd-c01a-46ea-b3e7-aee4180a764b"
        $array = $totalusers.objectid
     #>
       
       #Variables to check machines local memory and model against MDT DB 
        #$array2 = Get-EFMDTMakeModel | Select-Object model
        $Model = Get-WmiObject win32_computersystem | select model
        $bytes = (Get-WmiObject -class "cim_physicalmemory" | Measure-Object -Property Capacity -Sum).Sum
        $gb = $bytes / 1024 / 1024 / 1024
        $minmemory = "4"

        

            if ($UserInfo.username -eq $MessageTextbox1.Text)
                {
                    
                    #If users account typed inside the text box exists, it will output the message below"
                    $OutputBox1.text = "Username is valid"
                    $tick1.Image = $Tick
                    $MessageTextbox2.Text = $UserInfo.sitecode + "-" + $SerialKey
                    $TSEnv = New-Object -COMObject Microsoft.SMS.TSEnvironment 
                    $TSEnv.Value("OSDComputerName") = $MessageTextbox2.Text
                
           
                    if ($UserInfo.licenses.ems -eq "True")
                        {
                            
                            #If user account exists, it checks if its already a member of the default global group or sub groups "
                            $OutputBox2.text = "The user has valid license"
                            $tick2.Image = $Tick
                        }
                    else
                        {
                            
                            #User account exists but is not part of the default global or sub-groups and will add it to the gloabl group directly
                            $OutputBox2.text = "Could not add user to license group, please check with Global IT"
                            $tick2.Image = $Tick
                            #add-AzureADGroupmember -ObjectId $Global_AAD_Joined_Users_ObjectID -RefObjectId $UserAD_ObjectID.ObjectId
                        }
                 }
             else
                {
                    
                    #User is not present in ADD and will output this message, user will still be able to iniate the workspace but if they use
                    #the same user name at the azure AD join on the OOBE page, they wont have EMS license configured.
                    $OutputBox1.text = "Invalid Username"
                    $OutputBox2.text = "Please check Username"
                    $Cross1.Image = $Cross
                    $Cross2.Image = $Cross
                    [System.Windows.Forms.MessageBox]::Show("Invalid Username")
                    

                }

             if ($gb -ge $minmemory)
                {
                   
                   #Checks if it has minimum memory requirment, this is 4GB. If it does it will output the below message.
                   $OutputBox3.text = "Memory is sufficient"
                   $tick3.Image = $Tick
                }
           
             else
                {
                    
                    #Machine does not meet the minimum memory requirement and therefore cannot continue
                    $OutputBox3.text = "Memory is not sufficient"
                    $Cross3.Image = $Cross
                    [System.Windows.Forms.MessageBox]::Show("Memory is not sufficient, please check with Global IT")
                }

            if ($UserInfo.licenses.office -eq "True")
                {
                   
                   #Model info is not available in MDT DB, therefore not supported
                   $OutputBox4.text = "User has valid Office license"
                   $tick4.Image = $Tick
                }
           
             else
                {
                    #model is not supported
                    $OutputBox4.text = "User does not have valid Office license"
                    $Cross4.Image = $Cross
                    [System.Windows.Forms.MessageBox]::Show("Please make a nemo request for Offience license")
                }
            

            if ($OutputBox1.text -eq "Invalid Username" -or $OutputBox3.text -eq "Memory is not sufficient")
               {

                    $buttonStart.Enabled = $false
                    [System.Windows.Forms.MessageBox]::Show("Username or minimum memory requirement needs to be correct for the Cloud workspace to continue")

                }
            else
                {

                    $buttonStart.Enabled = $True

                }

        }

        elseif ($CheckBox.Checked -and !$MessageTextbox1.Text.Length) 
    {
            
            [System.Windows.Forms.MessageBox]::Show("Please type in a username")
            $CheckBox.Checked= $false

    }
        
      if ($CheckBox.Checked -ne $True)
        {

            #Output text will be cleared after un-ticking when checkbox has been ticked first
            $OutputBox1.text = ""
            $OutputBox2.text = ""
            $OutputBox3.text = ""
            $OutputBox4.text = ""

            #Remove images after checkbox is un-ticked  
            $WelcomeWindow.controls.Remove($Cross1)
            $WelcomeWindow.controls.Remove($Cross2)
            $WelcomeWindow.controls.Remove($Cross3)
            $WelcomeWindow.controls.Remove($Cross4)

            $WelcomeWindow.controls.Remove($tick1)
            $WelcomeWindow.controls.Remove($tick2)
            $WelcomeWindow.controls.Remove($tick3)
            $WelcomeWindow.controls.Remove($tick4)

            $Cross1.Image = $blank
            $Cross2.Image = $blank
            $Cross3.Image = $blank
            $Cross4.Image = $blank
            $tick1.Image = $blank
            $tick2.Image = $blank
            $tick3.Image = $blank
            $tick4.Image = $blank

            $buttonStart.Enabled = $false


        }               
    
}

    
    $CheckBox.Add_Click($CheckBox_OnClick)
    

#EndCheckBox________________________________________________________________________________________________________


#OutputBox1__________________________________________________________________________________________________________

    $OutputBox1.Font = New-Object System.Drawing.Font('Verdana',8,[System.Drawing.FontStyle]::Regular)
    $OutputBox1.ForeColor = "navy"
    $OutputBox1.AutoSize = $False
    $OutputBox1.Location = New-Object System.Drawing.Size (220,250)
    $OutputBox1.Size =  New-Object System.Drawing.Size(190,40)
    #$OutputBox1.MultiLine = $True
    $OutputBox1.BorderStyle = [System.Windows.Forms.BorderStyle]::None


#EndOfOutputBox1__________________________________________________________________________________________________________

#OutputBox2__________________________________________________________________________________________________________

    $OutputBox2.Font = New-Object System.Drawing.Font('Verdana',8,[System.Drawing.FontStyle]::Regular)
    $OutputBox2.ForeColor = "navy"
    $OutputBox2.AutoSize = $False
    $OutputBox2.Location = New-Object System.Drawing.Size (220,300)
    $OutputBox2.Size =  New-Object System.Drawing.Size(190,50)
    #$OutputBox2.MultiLine = $True
    $OutputBox2.BorderStyle = [System.Windows.Forms.BorderStyle]::None


#EndOfOutputBox2__________________________________________________________________________________________________________

#OutputBox3__________________________________________________________________________________________________________

    $OutputBox3.Font = New-Object System.Drawing.Font('Verdana',8,[System.Drawing.FontStyle]::Regular)
    $OutputBox3.ForeColor = "navy"
    $OutputBox3.AutoSize = $False
    $OutputBox3.Location = New-Object System.Drawing.Size (220,370)
    $OutputBox3.Size =  New-Object System.Drawing.Size(190,40)
    #$OutputBox3.MultiLine = $True
    $OutputBox3.BorderStyle = [System.Windows.Forms.BorderStyle]::None


#EndOfOutputBox3__________________________________________________________________________________________________________

#OutputBox4__________________________________________________________________________________________________________

    $OutputBox4.Font = New-Object System.Drawing.Font('Verdana',8,[System.Drawing.FontStyle]::Regular)
    $OutputBox4.ForeColor = "navy"
    $OutputBox4.AutoSize = $False
    $OutputBox4.Location = New-Object System.Drawing.Size (220,450)
    $OutputBox4.Size =  New-Object System.Drawing.Size(190,40)
    #$OutputBox4.MultiLine = $True
    $OutputBox4.BorderStyle = [System.Windows.Forms.BorderStyle]::None


#EndOfOutputBox4__________________________________________________________________________________________________________#>


#LabelText1______________________________________________________________________________________________________

    $Label1.Font = New-Object System.Drawing.Font('Verdana',8,[System.Drawing.FontStyle]::Regular)
    $Label1.AutoSize = $True
    $Label1.Location = New-Object System.Drawing.Size(45,250)
    $Label2.Size = New-Object System.Drawing.Size(170,70)
    $Label1.ForeColor = "navy"
    $Label1.Text = "Checking Username"

#EndOfLabelText1_________________________________________________________________________________________________

#LabelText2______________________________________________________________________________________________________

    $Label2.Font = New-Object System.Drawing.Font('Verdana',8,[System.Drawing.FontStyle]::Regular)
    $Label2.AutoSize = $False
    $Label2.Location = New-Object System.Drawing.Size(45,300)
    $Label2.Size = New-Object System.Drawing.Size(170,70)
    $Label2.ForeColor = "navy"
    $Label2.Text = "Username already exists or added to License group"

#EndOfLabelText2_________________________________________________________________________________________________

#LabelText3______________________________________________________________________________________________________

    $Label3.Font = New-Object System.Drawing.Font('Verdana',8,[System.Drawing.FontStyle]::Regular)
    $Label3.AutoSize = $False
    $Label3.Location = New-Object System.Drawing.Size(45,370)
    $Label3.Size = New-Object System.Drawing.Size(180,70)
    $Label3.ForeColor = "navy"
    $Label3.Text = "Checking Windows 10 compatibality"

#EndOfLabelText3_________________________________________________________________________________________________

#LabelText4______________________________________________________________________________________________________

    $Label4.Font = New-Object System.Drawing.Font('Verdana',8,[System.Drawing.FontStyle]::Regular)
    $Label4.AutoSize = $False
    $Label4.Location = New-Object System.Drawing.Size(45,440)
    $Label4.Size = New-Object System.Drawing.Size(170,40)
    $Label4.ForeColor = "navy"
    $Label4.Text = "Checking Office license"

#EndOfLabelText4_________________________________________________________________________________________________#>


#StartlButton_____________________________________________________________________________________________________

    $buttonStart.Location = New-Object System.Drawing.Size (150,580)
    $buttonStart.Size = New-Object System.Drawing.Size (100,40)
    $buttonStart.Font = New-Object System.Drawing.Font('Verdana',10,[System.Drawing.FontStyle]::Regular)
    $buttonStart.ForeColor = "navy"
    $buttonStart.Name = 'Install'
    $buttonStart.DialogResult = 'Yes'
    $buttonStart.Cursor = [System.Windows.Forms.Cursors]::Hand
    $buttonStart.Text = 'Start'
    $buttonStart.Enabled = $False

#EndOfStartButton________________________________________________________________________________________________

<#TraditionalButton_____________________________________________________________________________________________________

    $buttonTraditional.Location = New-Object System.Drawing.Size (480,580)
    $buttonTraditional.Size = New-Object System.Drawing.Size (100,40)
    $buttonTraditional.Font = New-Object System.Drawing.Font('Verdana',10,[System.Drawing.FontStyle]::Regular)
    $buttonTraditional.ForeColor = "navy"
    $buttonTraditional.Name = 'Traditional'
    $buttonTraditional.DialogResult = 'OK'
    $buttonTraditional.Cursor = [System.Windows.Forms.Cursors]::Hand
    $buttonTraditional.Text = 'Traditional'
    
    #$buttonStart.Enabled = $False

#EndOfTraditionalButton________________________________________________________________________________________________#>


#Cross___________________________________________________________________________________________________

    [System.Windows.Forms.Application]::EnableVisualStyles()
    $Cross1.DataBindings.DefaultDataSourceUpdateMode = 0
    $Cross2.DataBindings.DefaultDataSourceUpdateMode = 0
    $Cross3.DataBindings.DefaultDataSourceUpdateMode = 0
    $Cross4.DataBindings.DefaultDataSourceUpdateMode = 0
    $Cross1.Location =  New-Object System.Drawing.Size(480,250)
    $Cross2.Location =  New-Object System.Drawing.Size(480,300)
    $Cross3.Location =  New-Object System.Drawing.Size(480,350)
    $Cross4.Location =  New-Object System.Drawing.Size(480,450)
    $Cross1.Width = $Cross.Size.Width
    $Cross2.Width = $Cross.Size.Width
    $Cross3.Width = $Cross.Size.Width
    $Cross4.Width = $Cross.Size.Width
    $Cross1.Height = 30
    $Cross2.Height = 30
    $Cross3.Height = 30
    $Cross4.Height = 30
    #$Cross1.Image = $Cross
    #$Cross2.Image = $Cross
    #$Cross3.Image = $Cross
    #$Cross4.Image = $Cross

#Cross______________________________________________________________________________________________

#Tick___________________________________________________________________________________________________

    [System.Windows.Forms.Application]::EnableVisualStyles()
    $tick1.DataBindings.DefaultDataSourceUpdateMode = 0
    $tick2.DataBindings.DefaultDataSourceUpdateMode = 0
    $tick3.DataBindings.DefaultDataSourceUpdateMode = 0
    $tick4.DataBindings.DefaultDataSourceUpdateMode = 0
    $tick1.Location =  New-Object System.Drawing.Size(550,250)
    $tick2.Location =  New-Object System.Drawing.Size(550,300)
    $tick3.Location =  New-Object System.Drawing.Size(550,350)
    $tick4.Location =  New-Object System.Drawing.Size(550,450)
    $tick1.Width = $tick.Size.Width
    $tick2.Width = $tick.Size.Width
    $tick3.Width = $tick.Size.Width
    $tick4.Width = $tick.Size.Width
    $tick1.Height = 30
    $tick2.Height = 30
    $tick3.Height = 30
    $tick4.Height = 30
    #$tick1.Image = $tick
    #$tick2.Image = $tick
    #$tick3.Image = $tick
    #$tick4.Image = $tick

#Tick______________________________________________________________________________________________




#CancelButton_______________________________________________________________________________________________________

    $buttonCancel.Location = New-Object System.Drawing.Size (360,580)
    $buttonCancel.Size = New-Object System.Drawing.Size (100,40)
    $buttonCancel.Font = New-Object System.Drawing.Font('Verdana',10,[System.Drawing.FontStyle]::Regular)
    $buttonCancel.ForeColor = "navy"
    $buttonCancel.Name = 'Delay'
    $buttonCancel.DialogResult = 'No'
    $buttonCancel.Cursor = [System.Windows.Forms.Cursors]::Hand
    $buttonCancel.Text = 'Cancel'

#EndOfCancelButton__________________________________________________________________________________________________



#MainActions_______________________________________________________________________________________________________

    #Spawning Window 
    $WelcomeWindow.Add_Shown($WelcomeWindow.Activate())
    $ReturnValue = $WelcomeWindow.ShowDialog()
    $WelcomeWindow.Dispose()
    if ($ReturnValue -eq 'Yes') {
        Write-Host 'Install'
    }
    Elseif ($ReturnValue -eq 'No') {
        Start-Sleep 10 ; Restart-Computer -Confirm:$false -Force
    }
    elseif ($ReturnValue -eq 'OK')
    {
      start-process -WindowStyle Hidden  -Filepath "x:\LoadFrontend.exe" -Wait
           
    }
    

   

#MainActions_______________________________________________________________________________________________________